package avlTree;

public class AVLTree<T extends Comparable<T>> implements IAVLTree<T> {

	private BinaryNode<T> treeRoot;
	private int size;

	public AVLTree() {
		treeRoot = null;
		size = 0;
	}

	@Override
	public void insert(T key) {

		if (key == null) {
			return;
		} else {
			treeRoot = insert(treeRoot, key);
		}
	}

	private BinaryNode<T> insert(BinaryNode<T> node, T key) {
		if (node == null) {
			size++;
			return new BinaryNode<T>(key);
		}

		if (key.compareTo(node.getValue()) < 0) {
			BinaryNode<T> returnnode = insert(node.getLeftChild(), key);
			node.setLeftChild(returnnode);
		} else if (key.compareTo(node.getValue()) > 0) {
			BinaryNode<T> returnnode = insert(node.getRightChild(), key);
			node.setRightChild(returnnode);
		}
		int balanceDir = isBalanced(node);
		if (Math.abs(balanceDir) > 1) {
			node = balance(node, balanceDir);
		}
		node.setHeight(calculateHeight(node));
		return node;
	}

	@Override
	public boolean delete(T key) {

		int beforeInsert = size;
		treeRoot = delete(treeRoot, key);
		int afterInsert = size;
		return (afterInsert != beforeInsert);
	}

	private BinaryNode<T> delete(BinaryNode<T> root, T key) {

		if (root == null) {
			return null;
		}
		if (key.compareTo(root.getValue()) == 0) {
			if (root.getRightChild() == null) {
				root = root.getLeftChild();
			} else if (root.getLeftChild() == null) {
				root = root.getRightChild();
			} else {
				BinaryNode<T> min = successor(root);
				BinaryNode<T> temp = min.getRightChild();
				BinaryNode<T> r = root.getRightChild();
				BinaryNode<T> l = root.getLeftChild();
				r.setLeftChild(temp);
				int balanceDir = isBalanced(r);
				if (Math.abs(balanceDir) > 1) {
					r = balance(r, balanceDir);
				}
				r.setHeight(calculateHeight(r));
				root = min;
				if (min != r)
				root.setRightChild(r);
				root.setLeftChild(l);
			}
			size--;
		} else if (key.compareTo(root.getValue()) < 0) {
			root.setLeftChild(delete(root.getLeftChild(), key));

		} else {
			root.setRightChild(delete(root.getRightChild(), key));
		}
		if (root != null) {
			int balanceDir = isBalanced(root);
			if (Math.abs(balanceDir) > 1) {
				root = balance(root, balanceDir);
			}
			root.setHeight(calculateHeight(root));
		}
		return root;
	}

	@Override
	public boolean search(T key) {
		BinaryNode<T> current = treeRoot;
		while (current != null) {
			if (key.compareTo(current.getValue()) == 0) {
				return true;
			} else if (key.compareTo(current.getValue()) < 0) {
				current = current.getLeftChild();
			} else {
				current = current.getRightChild();
			}
		}
		return false;
	}

	/* starting from 1 */
	@Override
	public int height() {
		if (treeRoot != null) {
			return treeRoot.getHeight();
		}
		return 0;
	}

	public int size() {
		return size;
	}

	@Override
	public INode<T> getTree() {
		return treeRoot;
	}

	public BinaryNode<T> successor(BinaryNode<T> root) {
		return minimum((BinaryNode<T>) root.getRightChild());
	}

	public BinaryNode<T> predeccessor(BinaryNode<T> root) {
		return maximum((BinaryNode<T>) root.getLeftChild());
	}

	public BinaryNode<T> minimum(BinaryNode<T> root) {

		BinaryNode<T> current = root;
		while (current.getLeftChild() != null) {
			current = current.getLeftChild();
		}
		return current;
	}

	public BinaryNode<T> maximum(BinaryNode<T> root) {

		BinaryNode<T> current = root;
		while (current.getLeftChild() != null) {
			current = current.getRightChild();
		}
		return current;
	}

	public void traverseInOrder(BinaryNode<T> root) {
		if (root == null) {
			return;
		}
		traverseInOrder(root.getLeftChild());
		System.out.println(root.getValue() + "\n");
		traverseInOrder(root.getRightChild());
	}

	private BinaryNode<T> balance(BinaryNode<T> node, int dir) {

		/* single right rotation */
		if (isBalanced(node) > 0) {
			if (isBalanced(node.getLeftChild()) > 0) {
				return rotateRight(node);
			} else {
				BinaryNode<T> pivotR = rotateLeft(node.getLeftChild());
				node.setLeftChild(pivotR);
				node.setHeight(calculateHeight(node));
				return rotateRight(node);
			}
		} else {
			if (isBalanced(node.getRightChild()) < 0)
				return rotateLeft(node);
			else {
				BinaryNode<T> pivotL = rotateRight(node.getRightChild());
				node.setRightChild(pivotL);
				node.setHeight(calculateHeight(node));
				return rotateLeft(node);
			}
		}
	}

	private BinaryNode<T> rotateRight(BinaryNode<T> node) {
		// System.out.println(node.getValue());
		BinaryNode<T> pivot, temp2;
		pivot = node.getLeftChild();
		temp2 = pivot.getRightChild();
		pivot.setRightChild(node);
		node.setLeftChild(temp2);
		node.setHeight(calculateHeight(node));
		pivot.setHeight(calculateHeight(pivot));

		return pivot;
	}

	private BinaryNode<T> rotateLeft(BinaryNode<T> node) {
		BinaryNode<T> pivot, temp2;
		pivot = node.getRightChild();
		temp2 = pivot.getLeftChild();
		pivot.setLeftChild(node);
		node.setRightChild(temp2);
		node.setHeight(calculateHeight(node));
		pivot.setHeight(calculateHeight(pivot));
		return pivot;
	}

	private int calculateHeight(BinaryNode<T> node) {
		int hLeft = 0, hRight = 0;
		BinaryNode<T> left = node.getLeftChild();
		BinaryNode<T> right = node.getRightChild();
		if (left != null) {
			hLeft = left.getHeight();
		}
		if (right != null) {
			hRight = right.getHeight();
		}
		return (Math.max(hLeft, hRight) + 1);
	}

	/* +ve means left heavy */
	private int isBalanced(BinaryNode<T> node) {

		int hLeft = 0, hRight = 0;
		BinaryNode<T> left = node.getLeftChild();
		BinaryNode<T> right = node.getRightChild();
		if (left != null) {
			hLeft = left.getHeight();
		}
		if (right != null) {
			hRight = right.getHeight();
		}

		return hLeft - hRight;
	}

	public static void main(String[] args) {
		AVLTree<Integer> tree = new AVLTree<Integer>();
		tree.insert(8);

		tree.insert(6);
		tree.insert(9);
		tree.insert(7);
		tree.insert(10);
		tree.traverseInOrder((BinaryNode<Integer>) tree.getTree());

		System.out.println(tree.delete(8) + "second tree" + "\n");
		tree.traverseInOrder((BinaryNode<Integer>) tree.getTree());
		System.out.println(((BinaryNode<Integer>) tree.getTree()).getHeight() + " ");
		//
		/*
		 * 
		 * System.out.println(((BinaryNode<T>)
		 * pivot.getLeftChild()).getHeight());
		 * System.out.println(((BinaryNode<T>)
		 * pivot.getRightChild()).getHeight());
		 */
	}
}
