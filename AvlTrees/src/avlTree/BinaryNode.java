package avlTree;

public  class BinaryNode <T extends Comparable<T>> implements INode <T>{
	
	private BinaryNode<T> leftChild, rightChild;
	private int height;
    private T key;
	
	
	public BinaryNode(T value){
		height = 1;
		key = value;
		leftChild = null;
		rightChild = null;
	}
	
	@Override
	public BinaryNode<T> getLeftChild() {
		return leftChild;
	}

	@Override
	public BinaryNode<T> getRightChild() {
		return rightChild;
	}

	@Override
	public void setValue(T value) {
		key = value;
	}
	
	public T getValue() {
		return key;
	}

  
	public void setRightChild (BinaryNode<T> r){
		rightChild = r;
	}
	
	public void setLeftChild (BinaryNode<T> l){
		leftChild = l;
	}

	public void setHeight (int height){
		this.height = height;
	}
	
	public int getHeight (){
		return height;
	}
}
