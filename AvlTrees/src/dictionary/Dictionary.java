package dictionary;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import avlTree.AVLTree;

public class Dictionary implements IDictionary{

	private AVLTree<String> dictionary = new AVLTree<String>();
	@Override
	public void load(File file) {
		try {
			Scanner loader = new Scanner(file);
			while (loader.hasNextLine()){
				String newWord = loader.nextLine();
				insert(newWord);
						
			}
			loader.close();
		} catch (FileNotFoundException e) {
		
			System.out.println("File NOt Found");
		}
		
	}

	@Override
	public boolean insert(String word) {
		
		int beforeInsert = dictionary.size();
			dictionary.insert(word);
		int afterInsert = dictionary.size();	
		
		return (afterInsert != beforeInsert);
		
	}

	@Override
	public boolean exists(String word) {
		
		return dictionary.search(word);
	}

	@Override
	public boolean delete(String word) {
		
		return dictionary.delete(word);
	}

	@Override
	public int size() {
		
		return dictionary.size();
	}

	@Override
	public int height() {
		
		return dictionary.height();
	}
	public static void main(String[] args) {
		Dictionary dic = new Dictionary();
		File file = new File ("dic.txt");
		dic.load(file);
		
	}
}
